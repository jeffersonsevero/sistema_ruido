<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Helpers\Convert;
use Illuminate\Support\Facades\Route;



Route::get('/', function(){


    return redirect()->route('admin.login');

});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function (){


    //formulario de login
    Route::get('/', 'AuthController@showLoginForm')->name('login');
    Route::post('login', 'AuthController@login')->name('login.do');



    //rotas protegidas
    Route::group(['middleware' => ['auth'] ], function (){
        Route::get('home', 'AuthController@home')->name('home');
//        Route::get('clientes', 'ClienteController@index')->name('clientes.index');
//
//        Route::get('clientes/criar-novo', 'ClienteController@create')->name('clientes.create');
//        Route::post('clientes/store', 'ClienteController@store')->name('clientes.store');




        Route::resource('clientes', 'ClienteController');
        Route::resource('vendedores', 'VendedorController');
        Route::resource('ponto-vendas', 'PontoVendaController');
        Route::resource('clinicas', 'ClinicaController');
        Route::resource('representantes', 'RepresentanteController');
        Route::resource('pets', 'PetController');

        Route::resource("locais", "LocalController");

        Route::post("save-data", "DadoController@saveData")->name("dados.save");

        Route::post("/locais/alerta", "LocalController@alerta")->name("local.alerta");

        Route::get("/relatorio", "RelatorioController@index")->name("relatorio.index");
        Route::get("/relatorio/show/{local}", "RelatorioController@show")->name("relatorio.show");

    });


    //logout
    Route::get('logout', 'AuthController@logout')->name('logout');



});


