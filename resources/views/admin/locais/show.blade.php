@extends('admin.master.master')

@section('content')




<section class="dash_content_app">

    <header class="dash_content_app_header">
        <h2 class="icon-user-plus">Captura de ruído</h2>


        @if($errors->all())
        @foreach($errors->all() as $error)

        @message(['color' => 'red'])
        <p class="icon-exclamation-circle"> {{$error}} </p>

        @endmessage

        @endforeach

        @endif


        <div class="dash_content_app_header_actions">

            <nav class="dash_content_app_breadcrumb">


                <ul>
                    <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
                    <li class="separator icon-angle-right icon-notext"></li>
                    <li><a href="{{ route('admin.locais.index') }}">Locais</a></li>
                    <li class="separator icon-angle-right icon-notext"></li>
                    <li><a href="{{ route('admin.locais.create') }}" class="text-orange">Cadastrar novo local</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>

    <div class="dash_content_app_box">

        <div class="container">

            <div class="row">


                <div class="col-md-6">



                    <h3 class="my-2 text-bold"> Local: <span class="badge badge-success"> {{ $local->nome }} </span>
                    </h3>
                    <h3 class="my-2 text-bold"> Tipo: <span class="badge badge-success"> {{ $local->tipo }} </span>
                    </h3>
                    <h3 class="my-2 text-bold"> Descrição: <span class="badge badge-success"> {{ $local->descricao }}
                        </span> </h3>




                    <div class="form-group">
                        <label class="text-bold" for="">Tipo de alerta</label>
                        <select name="opcao" id="opcao" class="form-control">
                            <option selected disabled> Escolha uma opção </option>
                            <option value="email">E-mail</option>
                            <option value="telegram">Telegram</option>
                        </select>

                    </div>



                    <div class="form-group">
                        <label class="text-bold" for="">Selecione o intervalo entre alertas</label>
                        <select name="alert-interval" id="alert-interval" class="form-control">
                            <option selected disabled> Escolha uma opção </option>
                            <option value="5">5 minutos</option>
                            <option value="7">7 minutos</option>
                            <option value="9">9 minutos</option>
                            <option value="12">12 minutos</option>
                            <option value="15">15 minutos</option>
                            <option value="17">17 minutos</option>


                        </select>

                    </div>



                    <div class="form-group">
                        <label class="text-bold" for="">Mandar alerta em quantos decibéis?</label>
                        <select name="db-alert" id="db-alert" class="form-control">
                            <option selected disabled> Escolha uma opção </option>
                            <option value="50">50</option>
                            <option value="51">51</option>
                            <option value="52">52</option>
                            <option value="53">53</option>
                            <option value="54">54</option>
                            <option value="55">55</option>
                            <option value="56">56</option>
                            <option value="57">57</option>
                            <option value="58">58</option>
                            <option value="59">59</option>
                            <option value="60">60</option>
                            <option value="61">61</option>
                            <option value="62">62</option>
                            <option value="63">63</option>
                            <option value="64">64</option>
                            <option value="65">65</option>
                            <option value="66">66</option>
                            <option value="67">67</option>
                            <option value="68">68</option>
                            <option value="69">69</option>
                            <option value="70">70</option>
                            <option value="71">71</option>
                            <option value="72">72</option>
                            <option value="73">73</option>
                            <option value="74">74</option>
                            <option value="75">75</option>
                            <option value="76">76</option>
                            <option value="77">77</option>
                            <option value="78">78</option>
                            <option value="79">79</option>
                            <option value="80">80</option>
                            <option value="81">81</option>
                            <option value="82">82</option>
                            <option value="83">83</option>
                            <option value="84">84</option>
                            <option value="85">85</option>
                            <option value="86">86</option>
                            <option value="87">87</option>
                            <option value="88">88</option>
                            <option value="89">89</option>
                            <option value="90">90</option>
                            <option value="91">91</option>
                            <option value="92">92</option>
                            <option value="93">93</option>
                            <option value="94">94</option>
                            <option value="95">95</option>
                            <option value="96">96</option>
                            <option value="97">97</option>
                            <option value="98">98</option>
                            <option value="99">99</option>
                            <option value="100">100</option>


                        </select>

                    </div>

                    <table class="table table-striped my-4">

                        <tr>
                            <th>Nível de ruído (DB)</th>
                            <th>Máxima esposição diária permissível</th>
                        </tr>


                        <tr>
                            <td> 81 </td>
                            <td> 20 horas e 10 minutos </td>
                        </tr>

                        <tr>
                            <td> 82 </td>
                            <td> 16 horas </td>
                        </tr>

                        <tr>
                            <td> 83 </td>
                            <td> 12 horas e 41 minutos </td>
                        </tr>

                        <tr>
                            <td> 84 </td>
                            <td> 10 horas e 04 minutos </td>
                        </tr>

                        <tr>
                            <td> 85 </td>
                            <td> 08 horas </td>
                        </tr>

                        <tr>
                            <td> 86 </td>
                            <td> 06 horas e 20 minutos </td>
                        </tr>

                        <tr>
                            <td> 87 </td>
                            <td> 05 horas </td>
                        </tr>

                        <tr>
                            <td> 88 </td>
                            <td> 04 horas </td>
                        </tr>


                        <tr>
                            <td> 89 </td>
                            <td> 03 horas e 10 minutos </td>
                        </tr>

                        <tr>
                            <td> 90 </td>
                            <td> 02 horas e 30 minutos </td>
                        </tr>

                        <tr>
                            <td> 91 </td>
                            <td> 02 horas </td>
                        </tr>

                        <tr>
                            <td> 92 </td>
                            <td> 01 horas e 45 minutos </td>
                        </tr>


                        <tr>
                            <td> 93 </td>
                            <td> 01 horas e 15 minutos </td>
                        </tr>

                        <tr>
                            <td> 94 </td>
                            <td> 01 hora </td>
                        </tr>
                        <tr>
                            <td> 95 </td>
                            <td> 47 minutos </td>
                        </tr>

                        <tr>
                            <td> 96 </td>
                            <td> 37 minutos </td>
                        </tr>

                        <tr>
                            <td> 97 </td>
                            <td> 30 minutos </td>
                        </tr>
                    </table>
                </div>

                <div class="col-md-6 d-flex flex-column  align-items-center">
                    <h1 class="text-bold icon-microphone"> Ruído em tempo real </h1>

                    <div id="box-ruido" class="d-flex flex-column justify-content-center align-items-center mt-1"
                        style="width: 300px; height: 400px; background-color:#2D9A81; border-radius: 400px">

                        <h1 class="text-white text-bold" id="db">0</h1>
                        <h1 class="text-white">Decibéis</h1>

                    </div>

                </div>


            </div>

        </div>

    </div>
</section>


@endsection





@section('js')

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });



    $(function() {
        var alertType = null;
        var interval = null;


        $("#opcao").change(function(){

            alertType = $(this).val();
        });

        $("#alert-interval").change(function(){

            interval = $(this).val();

        });


        function getMin(array) {

            let min = Math.min.apply(Math, array);

            return min;


        }

        function getAvg(array) {


            let sum = 0;

            for (var i = 0; i < array.length; i++) {
                sum += array[i];
            }
            let avg = parseFloat(sum / array.length);
            return avg.toFixed(2);

        }


        function getMax(array) {

            let max = Math.max.apply(Math, array);

            return max;

        }



        function sendData(minDb, maxDb, avgDb) {

            $.ajax({
                url: "{{ route('admin.dados.save') }}",
                 type: "POST",
                 data: {
                    min: minDb
                    , max: maxDb
                    , avg: avgDb
                    , local: '{{ $local->id }}'

                }

            }).done(function(response) {
                console.log(response);

            });


        }

        function sendAlert(data) {

            $.ajax({
                url: "{{ route('admin.local.alerta') }}"
                , method: "post"
                , data: data

            }).done(function(response) {
                console.log(response);

            });
        }

        const ws = new WebSocket('ws://192.168.1.102:4040');

        let dbs = [];

        var dbAlert = null;

        $("#db-alert").change(function(){

            dbAlert = parseInt ( $("#db-alert option:selected").val() );
            console.log(dbAlert);

        });

        ws.addEventListener("message", msg => {
            const span = document.querySelector("#db");
            span.classList.add("zoto");
            span.textContent = msg.data;
            console.log(msg.data);


            if(Number.isInteger(dbAlert)){
                console.log(dbAlert);

                if (msg.data > dbAlert) {

                    $("#box-ruido").css("background", "#f44336");
                    $("#box-ruido").css("background", "#2D9A81");


                let dados = {
                    db: msg.data
                    , local: "{{ $local->nome }}",
                    local_id: "{{ $local->id }}",
                    typeAlert: alertType

                }

                sendAlert(dados);

            }

            if(Number.isInteger(interval)){

                setTimeout(function(){

                }, interval * 60000);

            }



            }

            if (dbs.length < 120) {

                dbs.push(parseFloat(msg.data));

            } else {
                // dbs.shift();
                // dbs.push(parseFloat(msg.data));

                if (dbs.length > 0) {
                    sendData(getMin(dbs), getMax(dbs), getAvg(dbs))

                }

                dbs = [];

            }

            console.log(dbs);


        });

    });

</script>


@endsection
