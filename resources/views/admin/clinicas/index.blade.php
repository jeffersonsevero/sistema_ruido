
@extends('admin.master.master')

@section('content')

    <section class="dash_content_app">




        <header class="dash_content_app_header">
            <h2 class="icon-search">Filtro</h2>

            <div class="dash_content_app_header_actions">
                <nav class="dash_content_app_breadcrumb">
                    <ul>
                        <li><a href=" {{ route('admin.home') }}">Dashboard</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="{{ route('admin.clinicas.index') }}" class="text-orange">Clínicas</a></li>
                    </ul>
                </nav>

                <a href="{{ route('admin.clinicas.create') }}" class="btn btn-orange icon-user ml-1">Criar Clínica</a>
                <button class="btn btn-green icon-search icon-notext ml-1 search_open"></button>
            </div>
        </header>




        <div class="dash_content_app_box">
            <div class="dash_content_app_box_stage">
                <table id="dataTable" class="nowrap stripe" width="100" style="width: 100% !important;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Clínica</th>
                        <th>CNPJ</th>
                        <th>E-mail</th>
                        <th>Responsável</th>
                        <th>Ações</th>

                    </tr>
                    </thead>

                    <tbody>

                    @if(isset($clinicas))

                        @foreach($clinicas as $clinica)

                            <tr>
                                <td> {{$clinica->id}} </td>
                                <td><a class="text-green" href="{{ route('admin.clinicas.edit', ['clinica' => $clinica->id]) }}"> {{$clinica->nome}} </a> </td>
                                <td> {{$clinica->cnpj}} </td>
                                <td> {{$clinica->email}} </td>
                                <td><a class="text-green" href=""> {{ $clinica->responsavel }} </a> </td>

                                <td>
                                    <form method="post" action="{{ route('admin.clinicas.destroy', ['clinica' => $clinica->id])  }}" onsubmit="return confirm('Tem certeza que deseja excluir?')">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-red icon-times"></button>

                                    </form>

                                </td>
                            </tr>

                        @endforeach


                    @endif




                    </tbody>


                </table>
            </div>
        </div>
    </section>


@endsection
