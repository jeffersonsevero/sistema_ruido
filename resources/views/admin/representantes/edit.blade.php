@extends('admin.master.master')

@section('content')



    <section class="dash_content_app">

        <header class="dash_content_app_header">
            <h2 class="icon-user-plus">Editar Representante Comercial</h2>


            @if($errors->all())
                @foreach($errors->all() as $error)

                    @message(['color' => 'red'])
                    <p class="icon-exclamation-circle"> {{$error}} </p>

                    @endmessage

                @endforeach

            @endif




            <div class="dash_content_app_header_actions">

                <nav class="dash_content_app_breadcrumb">


                    <ul>
                        <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="{{ route('admin.representantes.index') }}">Representantes</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="{{ route('admin.representantes.create') }}" class="text-orange">Novo Representante</a></li>
                    </ul>
                </nav>
            </div>
        </header>

        <div class="dash_content_app_box">
            <div class="nav">
                <ul class="nav_tabs">
                    <li class="nav_tabs_item">
                        <a href="#data" class="nav_tabs_item_link active">Dados Cadastrais</a>
                    </li>

                    <li class="nav_tabs_item">
                        <a href="#endereco" class="nav_tabs_item_link active">Endereço</a>
                    </li>



                </ul>



                <form class="app_form" action="{{ route('admin.representantes.update', ['representante' => $representante]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="nav_tabs_content">
                        <div id="data">


                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">*Nome:</span>
                                    <input type="text" name="name" placeholder="Nome completo" value="{{ old('name') ?? $representante->nome }}" />
                                </label>


                            </div>


                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">*E-mail:</span>
                                    <input type="text" name="email" placeholder="E-mail" value="{{ old('email') ?? $representante->email }}" />
                                </label>

                            </div>

                            <div class="label_g2">

                                <label class="label">
                                    <span class="legend">*CPF:</span>
                                    <input type="tel" class="mask-doc" name="cpf" placeholder="CPF do Vendedor" value="{{ old('cpf') ?? $representante->cpf }}" />
                                </label>
                            </div>


                            <div class="label_g2">

                                <label class="label">
                                    <span class="legend">*RG:</span>
                                    <input type="tel" class="" name="rg" placeholder="RG Vendedor" value="{{ old('rg') ?? $representante->rg }}" />
                                </label>
                            </div>


                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">Telefone:</span>
                                    <input class="mask-phone" type="text" name="telefone" placeholder="Telefone" value="{{ old('telefone') ?? $representante->telefone }}" />
                                </label>

                            </div>

                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">Whatsapp:</span>
                                    <input class="mask-phone" type="text" name="whats" placeholder="WhatSapp" value="{{ old('whats') ?? $representante->whats }}" />
                                </label>

                            </div>



                            <label class="label">
                                <span class="legend">Possui Habilitação:</span>
                                <select name="habilitacao" class="select2">
                                    <option value="1" {{  ($representante->habilitacao == 1 ? 'selected' : ''   )  }}>Sim
                                    </option>
                                    <option value="0" {{  ($representante->habilitacao == 0 ? 'selected' : ''   )  }}>Não</option>
                                </select>
                            </label>

                        </div>

                        <div id="endereco" class="d-none">




                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">*CEP:</span>
                                    <input type="tel" name="cep" class="mask-zipcode zip_code_search" placeholder="Digite o CEP" value="{{ old('cep') ?? $representante->cep  }}" />
                                </label>
                            </div>

                            <label class="label">
                                <span class="legend">*Endereço:</span>
                                <input type="text" name="endereco" class="street" placeholder="Endereço Completo" value="{{ old('endereco') ?? $representante->rua }}" />
                            </label>

                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">*Número:</span>
                                    <input type="text" name="numero" placeholder="Número do Endereço" value="{{ old('numero') ?? $representante->numero }}" />
                                </label>

                                <label class="label">
                                    <span class="legend">Complemento:</span>
                                    <input type="text" name="complemento" placeholder="Completo (Opcional)" value="{{ old('complemento') ?? $representante->complemento }}" />
                                </label>
                            </div>

                            <label class="label">
                                <span class="legend">*Bairro:</span>
                                <input type="text" name="bairro" class="neighborhood" placeholder="Bairro" value="{{ old('bairro') ?? $representante->bairro }}" />
                            </label>

                            <div class="label_g2">
                                <label class="label">
                                    <span class="legend">*Estado:</span>
                                    <input type="text" name="estado" class="state" placeholder="Estado" value="{{ old('estado') ?? $representante->estado }}" />
                                </label>

                                <label class="label">
                                    <span class="legend">*Cidade:</span>
                                    <input type="text" name="cidade" class="city" placeholder="Cidade" value="{{ old('cidade') ?? $representante->cidade }}" />
                                </label>
                            </div>


                        </div>









                    </div>


                    <div id="realties" class="d-none">
                        <div class="app_collapse">
                            <div class="app_collapse_header collapse">
                                <h3>Locador</h3>
                                <span class="icon-minus-circle icon-notext"></span>
                            </div>

                            <div class="app_collapse_content">
                                <div id="realties">
                                    <div class="realty_list">
                                        <div class="realty_list_item mb-1">
                                            <div class="realty_list_item_actions_stats">
                                                <img src="assets/images/realty.jpeg" alt="">
                                                <ul>
                                                    <li>Venda: R$ 450.000,00</li>
                                                    <li>Aluguel: R$ 2.000,00</li>
                                                </ul>
                                            </div>

                                            <div class="realty_list_item_content">
                                                <h4>Casa Residencial - Campeche</h4>

                                                <div class="realty_list_item_card">
                                                    <div class="realty_list_item_card_image">
                                                        <span class="icon-realty-location"></span>
                                                    </div>
                                                    <div class="realty_list_item_card_content">
                                                        <span class="realty_list_item_description_title">Bairro:</span>
                                                        <span class="realty_list_item_description_content">Campeche</span>
                                                    </div>
                                                </div>

                                                <div class="realty_list_item_card">
                                                    <div class="realty_list_item_card_image">
                                                        <span class="icon-realty-util-area"></span>
                                                    </div>
                                                    <div class="realty_list_item_card_content">
                                                        <span class="realty_list_item_description_title">Área Útil:</span>
                                                        <span class="realty_list_item_description_content">150m&sup2;</span>
                                                    </div>
                                                </div>

                                                <div class="realty_list_item_card">
                                                    <div class="realty_list_item_card_image">
                                                        <span class="icon-realty-bed"></span>
                                                    </div>
                                                    <div class="realty_list_item_card_content">
                                                        <span class="realty_list_item_description_title">Domitórios:</span>
                                                        <span class="realty_list_item_description_content">4 Quartos<br><span>Sendo 2 suítes</span></span>
                                                    </div>
                                                </div>

                                                <div class="realty_list_item_card">
                                                    <div class="realty_list_item_card_image">
                                                        <span class="icon-realty-garage"></span>
                                                    </div>
                                                    <div class="realty_list_item_card_content">
                                                        <span class="realty_list_item_description_title">Garagem:</span>
                                                        <span class="realty_list_item_description_content">4 Vagas<br><span>Sendo 2 cobertas</span></span>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="realty_list_item_actions">
                                                <ul>
                                                    <li class="icon-eye">1234 Visualizações</li>
                                                </ul>
                                                <div>
                                                    <a href="" class="btn btn-blue icon-eye">Visualizar Imóvel</a>
                                                    <a href="" class="btn btn-green icon-pencil-square-o">Editar
                                                        Imóvel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="no-content">Não foram encontrados registros!</div>
                                </div>
                            </div>
                        </div>

                        <div class="app_collapse mt-3">
                            <div class="app_collapse_header collapse">
                                <h3>Locatário</h3>
                                <span class="icon-minus-circle icon-notext"></span>
                            </div>

                            <div class="app_collapse_content">
                                <div id="realties">
                                    <div class="no-content">Não foram encontrados registros!</div>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>

            <div class="text-right mt-2">
                <button class="btn btn-large btn-green icon-check-square-o" type="submit">Salvar Alterações
                </button>
            </div>
            </form>
        </div>
        </div>
    </section>

@endsection
