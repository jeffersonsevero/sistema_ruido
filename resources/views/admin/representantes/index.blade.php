
@extends('admin.master.master')

@section('content')

    <section class="dash_content_app">




        <header class="dash_content_app_header">
            <h2 class="icon-search">Filtro</h2>

            <div class="dash_content_app_header_actions">
                <nav class="dash_content_app_breadcrumb">
                    <ul>
                        <li><a href=" {{ route('admin.home') }}">Dashboard</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="{{ route('admin.representantes.index') }}" class="text-orange">Representantes Comerciais</a></li>
                    </ul>
                </nav>

                <a href="{{ route('admin.representantes.create') }}" class="btn btn-orange icon-user ml-1">Criar Representante</a>
                <button class="btn btn-green icon-search icon-notext ml-1 search_open"></button>
            </div>
        </header>




        <div class="dash_content_app_box">
            <div class="dash_content_app_box_stage">
                <table id="dataTable" class="nowrap stripe" width="100" style="width: 100% !important;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome Completo</th>
                        <th>CPF</th>
                        <th>Telefone</th>
                        <th>E-mail</th>
                        <th>Ações</th>

                    </tr>
                    </thead>

                    <tbody>

                    @if(isset($representantes))

                        @foreach($representantes as $representante)

                            <tr>
                                <td> {{$representante->id}} </td>
                                <td><a class="text-green" href="{{ route('admin.representantes.edit', ['representante' => $representante->id]) }}"> {{$representante->nome}} </a> </td>
                                <td> {{$representante->cpf}} </td>
                                <td> {{$representante->telefone}} </td>
                                <td><a class="text-green" href=""> {{ $representante->email }} </a> </td>

                                <td>
                                    <form method="post" action="{{ route('admin.representantes.destroy', ['representante' => $representante->id])  }}" onsubmit="return confirm('Tem certeza que deseja excluir?')">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-red icon-times"></button>

                                    </form>

                                </td>
                            </tr>

                        @endforeach


                    @endif




                    </tbody>


                </table>
            </div>
        </div>
    </section>


@endsection
