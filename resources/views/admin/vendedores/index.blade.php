
@extends('admin.master.master')

@section('content')

    <section class="dash_content_app">




        <header class="dash_content_app_header">
            <h2 class="icon-search">Filtro</h2>

            <div class="dash_content_app_header_actions">
                <nav class="dash_content_app_breadcrumb">
                    <ul>
                        <li><a href=" {{ route('admin.home') }}">Dashboard</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="{{ route('admin.vendedores.index') }}" class="text-orange">Vendedores</a></li>
                    </ul>
                </nav>

                <a href="{{ route('admin.vendedores.create') }}" class="btn btn-orange icon-user ml-1">Criar Vendedor</a>
                <button class="btn btn-green icon-search icon-notext ml-1 search_open"></button>
            </div>
        </header>




        <div class="dash_content_app_box">
            <div class="dash_content_app_box_stage">
                <table id="dataTable" class="nowrap stripe" width="100" style="width: 100% !important;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome Completo</th>
                        <th>CPF</th>
                        <th>Telefone</th>
                        <th>E-mail</th>
                        <th>Ações</th>

                    </tr>
                    </thead>

                    <tbody>

                    @if(isset($vendedores))

                        @foreach($vendedores as $vendedor)

                            <tr>
                                <td> {{$vendedor->id}} </td>
                                <td><a class="text-green" href="{{ route('admin.vendedores.edit', ['vendedor' => $vendedor->id]) }}"> {{$vendedor->nome}} </a> </td>
                                <td> {{$vendedor->cpf}} </td>
                                <td> {{$vendedor->telefone}} </td>
                                <td><a class="text-green" href=""> {{ $vendedor->email }} </a> </td>

                                <td>
                                    <form method="post" action="{{ route('admin.vendedores.destroy', ['vendedor' => $vendedor->id])  }}" onsubmit="return confirm('Tem certeza que deseja excluir?')">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-red icon-times"></button>

                                    </form>

                                </td>
                            </tr>

                        @endforeach


                    @endif




                    </tbody>


                </table>
            </div>
        </div>
    </section>


@endsection
