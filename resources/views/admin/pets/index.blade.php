
@extends('admin.master.master')

@section('content')

    <section class="dash_content_app">




        <header class="dash_content_app_header">
            <h2 class="icon-search">Filtro</h2>

            <div class="dash_content_app_header_actions">
                <nav class="dash_content_app_breadcrumb">
                    <ul>
                        <li><a href=" {{ route('admin.home') }}">Dashboard</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="{{ route('admin.pets.index') }}" class="text-orange">Pets</a></li>
                    </ul>
                </nav>

                <a href="{{ route('admin.pets.create') }}" class="btn btn-orange icon-user ml-1">Criar Pet</a>
                <button class="btn btn-green icon-search icon-notext ml-1 search_open"></button>
            </div>
        </header>


        <div class="dash_content_app_box">
            <div class="dash_content_app_box_stage">
                <table id="dataTable" class="nowrap stripe" width="100" style="width: 100% !important;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome </th>
                        <th>Dono</th>
                        <th>Raça</th>
                        <th>Idade</th>
                        <th>Ações</th>

                    </tr>
                    </thead>

                    <tbody>

                    @if(isset($pets))

                        @foreach($pets as $pet)


                            <tr>
                                <td> {{$pet->id}} </td>
                                <td> {{$pet->nome}} </td>
                                <td> {{$pet->dono["name"]}} </td>
                                <td> {{$pet->raca}} </td>
                                <td> {{$pet->idade}} </td>

                                <td>
                                    <form method="post" action="{{ route('admin.pets.destroy', ['pet' => $pet->id])  }}" onsubmit="return confirm('Tem certeza que deseja excluir?')">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-red icon-times"></button>

                                    </form>

                                </td>
                            </tr>

                        @endforeach


                    @endif




                    </tbody>


                </table>
            </div>
        </div>
    </section>


@endsection
