@extends('admin.master.master')

@section('content')



    <section class="dash_content_app">

        <header class="dash_content_app_header">
            <h2 class="icon-user-plus">Novo Pet</h2>


            @if($errors->all())
                @foreach($errors->all() as $error)

                    @message(['color' => 'red'])
                    <p class="icon-exclamation-circle"> {{$error}} </p>

                    @endmessage

                @endforeach

            @endif


            <div class="dash_content_app_header_actions">

                <nav class="dash_content_app_breadcrumb">


                    <ul>
                        <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="{{ route('admin.pets.index') }}">Pets</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="{{ route('admin.pets.create') }}" class="text-orange">Novo Pet</a></li>
                    </ul>
                </nav>
            </div>
        </header>

        <div class="dash_content_app_box">
            <div class="nav">
                <ul class="nav_tabs">
                    <li class="nav_tabs_item">
                        <a href="#data" class="nav_tabs_item_link active">Dados Cadastrais</a>
                    </li>


                </ul>

                <form class="app_form" action="{{ route('admin.pets.store') }}" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="nav_tabs_content">
                        <div id="data">






                            <div class="app_collapse mt-2">
                                <div class="app_collapse_header collapse">
                                    <h3>Pet</h3>
                                    <span class="icon-plus-circle icon-notext"></span>
                                </div>

                                <div class="app_collapse_content d-none">

                                    <div class="label_g2">
                                        <label class="label">
                                            <span class="legend">*Nome:</span>
                                            <input type="text" name="nome_pet" placeholder="Nome do pet"
                                                   value="{{ old('nome_pet') }}"/>
                                        </label>



                                        <label class="label">
                                            <span class="legend">*Raça:</span>
                                            <input type="text" name="raca_pet"
                                                   placeholder="Raça do pet" value="{{ old('raca_pet') }}"/>
                                        </label>
                                    </div>

                                    <div class="label_g2">
                                        <label class="label">
                                            <span class="legend">*Idade:</span>
                                            <input type="text" name="idade"
                                                   placeholder="Idade do Pet" value="{{ old('idade') }}"/>
                                        </label>

                                    </div>


                                    <div class="label_g2">
                                        <label class="label">
                                            <span class="legend">*Dono:</span>
                                            <select name="dono" id="dono">

                                                @if(!empty($clientes))

                                                    @foreach($clientes as $cliente)
                                                        <option value="{{ $cliente->id }}"> {{ $cliente->nome }} </option>

                                                    @endforeach

                                                @endif

                                            </select>

                                        </label>

                                        <label class="label">
                                            <span class="legend">*Data de nascimento:</span>
                                            <input type="text" name="data_nascimento_pet" class="mask-date"
                                                   placeholder="Data de nascimento" value="{{ old('data_nascimento_pet') }}"/>
                                        </label>
                                    </div>


                                    <div class="label_g2">

                                        <label class="label">
                                            <span class="legend">*Data de aquisição:</span>
                                            <input class="mask-date" type="text" name="data_aquisicao_pet" placeholder="Data de aquisição"
                                                   value="{{ old('data_aquisicao_pet') }}"/>
                                        </label>


                                        <label class="label">
                                            <span class="legend">Doenças crônicas:</span>
                                            <input type="text" name="doencas_cronicas_pet" placeholder="Doenças crônicas"
                                                   value="{{ old('doencas_cronicas_pet') }}"/>
                                        </label>


                                    </div>


                                    <div class="label_g2">

                                        <label class="label">
                                            <span class="legend">Deficiência:</span>
                                            <input  type="text" name="deficiencia_pet" placeholder="Deficiência(s)"
                                                   value="{{ old('deficiencia_pet') }}"/>
                                        </label>


                                        <label class="label">
                                            <span class="legend">*Imagem:</span>
                                            <input type="file" name="imagem_pet"
                                                   value="{{ old('imagem_pet') }}"/>
                                        </label>


                                    </div>


                                </div>
                            </div>



                        </div>


                    </div>

                    <div class="text-right mt-2">
                        <button class="btn btn-large btn-green icon-check-square-o" type="submit">Salvar Alterações
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </section>

@endsection
