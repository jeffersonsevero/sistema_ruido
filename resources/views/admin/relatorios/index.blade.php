
@extends('admin.master.master')

@section('content')

    <section class="dash_content_app">




        <header class="dash_content_app_header">
            <h2 class="icon-search">Filtro</h2>

            <div class="dash_content_app_header_actions">
                <nav class="dash_content_app_breadcrumb">
                    <ul>
                        <li><a href=" {{ route('admin.home') }}">Dashboard</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="{{ route('admin.locais.index') }}" class="text-orange">Locais</a></li>
                    </ul>
                </nav>

                <a href="{{ route('admin.locais.create') }}" class="btn btn-orange icon-user ml-1">Cadastrar novo local</a>
                <button class="btn btn-green icon-search icon-notext ml-1 search_open"></button>
            </div>
        </header>




        <div class="dash_content_app_box">
            <div class="dash_content_app_box_stage">
                <table id="dataTable" class="nowrap stripe" width="100" style="width: 100% !important;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>Tipo</th>
                        <th>Descrição</th>
                        <th>Ações</th>

                    </tr>
                    </thead>

                    <tbody>

                    @if(isset($locais))

                        @foreach($locais as $local)

                            <tr>
                                <td> {{$local->id}} </td>
                                <td><a class="text-green" href="{{ route('admin.locais.edit', ['local' => $local->id]) }}"> {{$local->nome}} </a> </td>
                                <td> {{$local->tipo}} </td>
                                <td> {{$local->descricao}} </td>

                                <td>

                                    <a class="btn btn-success" href=" {{ route('admin.relatorio.show', ['local' => $local->id] ) }} ">Ver relatório</a>

                                </td>
                            </tr>

                        @endforeach


                    @endif


                    </tbody>


                </table>
            </div>
        </div>
    </section>


@endsection
