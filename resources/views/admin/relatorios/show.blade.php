@extends('admin.master.master')



@section('content')


@section('js')

<script src="https://cdn.jsdelivr.net/npm/chart.js@3.4.1/dist/chart.min.js"></script>

<script>

var ctx = document.getElementById('myChart').getContext('2d');



var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [ <?= $chart->times ?> ],
        datasets: [

            {
                label: "Decibéis",
                data: [ <?= $chart->dbs ?> ],
                borderWidth: 2,
                borderColor: 'rgba(77, 176, 253, 0.85)',
                backgroundColor: 'transparent'
            },

            {
                label: "Limiar de ruído para esse tipo de atividade",
                data: [ <?=  $chart->max  ?> ],
                borderWidth: 2,
                borderColor: 'rgba(255, 99, 71, 0.5)',
                backgroundColor: 'transparent'
            },

        ],

    },
    options: {

        plugins: {
            title: {
                display: true,
                text: 'Relatório de ruído - {{ $local->nome }}'
            }
        },

        scales: {
            y: {
                title: {
                    display: true,
                    text: 'Decibéis'
                },
                beginAtZero: true,


            },

            x: {
                title: {
                    display: true,
                    text: 'Tempo'
                },
                beginAtZero: true,


            },




        }
    }
});



</script>


@endsection

<section class="dash_content_app">

    <header class="dash_content_app_header">
        <h2 class="icon-user-plus"> Local: {{ $local->nome }} </h2>


        @if($errors->all())
        @foreach($errors->all() as $error)

        @message(['color' => 'red'])
        <p class="icon-exclamation-circle"> {{$error}} </p>

        @endmessage

        @endforeach

        @endif


        <div class="dash_content_app_header_actions">

            <nav class="dash_content_app_breadcrumb">


                <ul>
                    <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
                    <li class="separator icon-angle-right icon-notext"></li>
                    <li><a href="{{ route('admin.relatorio.index') }}"> Relatórios </a></li>
                    <li class="separator icon-angle-right icon-notext"></li>
                    </li>
                </ul>
            </nav>
        </div>
    </header>

    <div class="dash_content_app_box">

        <div class="container-fluid">

            <div class="row">

                <div class="col-md-6">


                    <div class="dash_content_app_box_stage">

                        <table id="dataTable" class="nowrap stripe" width="100" style="width: 100% !important;">

                            <thead>
                                <tr>
                                    <th>Média em DB</th>
                                    <th> Dia/Horário </th>

                                </tr>
                            </thead>

                            <tbody>

                                @if(isset($data))

                                @foreach($data as $dataItem)

                                <tr>

                                    <td> {{$dataItem->avg_db}} </td>
                                    <td> {{ convert_to_date_br($dataItem->created_at) }} </td>

                                </tr>

                                @endforeach


                                @endif

                            </tbody>


                        </table>
                    </div>


                </div>

                <div class="col-md-5 text-center">

                    <h2 class="text-center my-3 badge badge-info"> Desvio padrão: {{ $chart->standardDeviation }} </h2>
                    <h2 class="text-center my-3 badge badge-info"> Média: {{ $chart->avg }} </h2>
                    <canvas id="myChart" width="100" height="100"></canvas>


                </div>



            </div>



        </div>

    </div>
</section>

@endsection




@section('js')




<script>




</script>

@endsection
