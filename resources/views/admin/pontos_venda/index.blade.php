
@extends('admin.master.master')

@section('content')

    <section class="dash_content_app">




        <header class="dash_content_app_header">
            <h2 class="icon-search">Filtro</h2>

            <div class="dash_content_app_header_actions">
                <nav class="dash_content_app_breadcrumb">
                    <ul>
                        <li><a href=" {{ route('admin.home') }}">Dashboard</a></li>
                        <li class="separator icon-angle-right icon-notext"></li>
                        <li><a href="{{ route('admin.ponto-vendas.index') }}" class="text-orange">Pontos de venda</a></li>
                    </ul>
                </nav>

                <a href="{{ route('admin.ponto-vendas.create') }}" class="btn btn-orange icon-user ml-1">Criar Ponto de Venda</a>
                <button class="btn btn-green icon-search icon-notext ml-1 search_open"></button>
            </div>
        </header>




        <div class="dash_content_app_box">
            <div class="dash_content_app_box_stage">
                <table id="dataTable" class="nowrap stripe" width="100" style="width: 100% !important;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>CNPJ</th>
                        <th>E-mail</th>
                        <th>Responsável</th>
                        <th>Ações</th>

                    </tr>
                    </thead>

                    <tbody>

                    @if(isset($pontos))

                        @foreach($pontos as $ponto)

                            <tr>
                                <td> {{$ponto->id}} </td>
                                <td><a class="text-green" href="{{ route('admin.ponto-vendas.edit', ['ponto' => $ponto->id]) }}"> {{$ponto->nome}} </a> </td>
                                <td> {{$ponto->cnpj}} </td>
                                <td> {{$ponto->email}} </td>
                                <td><a class="text-green" href=""> {{ $ponto->responsavel }} </a> </td>

                                <td>
                                    <form method="post" action="{{ route('admin.ponto-vendas.destroy', ['ponto' => $ponto->id])  }}" onsubmit="return confirm('Tem certeza que deseja excluir?')">
                                        @method('DELETE')
                                        @csrf
                                        <button class="btn btn-red icon-times"></button>

                                    </form>

                                </td>
                            </tr>

                        @endforeach


                    @endif




                    </tbody>


                </table>
            </div>
        </div>
    </section>


@endsection
