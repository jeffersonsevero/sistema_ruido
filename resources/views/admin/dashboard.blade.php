@extends('admin.master.master')

@section('content')




    <div style="flex-basis: 100%;">
        <section class="dash_content_app">
            <header class="dash_content_app_header">
                <h2 class="icon-tachometer">Dashboard</h2>
            </header>

            <div class="dash_content_app_box">
                <section class="app_dash_home_stats">
                    <article class="control radius">
                        <h4 class="icon-users">Usuários</h4>
                        <p><b>Quantidade:</b>  {{ $users->count() }}  </p>

                    </article>

                    <article class="blog radius">
                        <h4 class="icon-home">Locais</h4>
                        <p><b>Quantidade:</b> {{ $places->count() }}  </p>

                    </article>

                    <article class="users radius">
                        <h4 class="icon-bell">Alarmes hoje</h4>
                        <p><b>Quantidade:</b>  {{ $alerts->count() }}   </p>
                    </article>
                </section>
            </div>
        </section>

        <section class="dash_content_app" style="margin-top: 40px;">
            <header class="dash_content_app_header">
                <h2 class="icon-tachometer">Últimas informações cadastradas</h2>
            </header>

            <div class="dash_content_app_box">
                <div class="dash_content_app_box_stage">
                    <table id="dataTable" class="nowrap hover stripe" width="100" style="width: 100% !important;">
                        <thead>
                        <tr>
                            <th>Local</th>
                            <th>Mínimo DB</th>
                            <th>Máximo DB</th>
                            <th>Média DB</th>
                        </tr>
                        </thead>
                        <tbody>


                        @if(isset($data))

                            @foreach($data as $dataItem)


                                <tr>
                                    <td> {{  $dataItem->placeName->nome }} </td>
                                    <td> {{  $dataItem->min_db }} </td>
                                    <td>  {{ $dataItem->max_db }} </td>
                                    <td>  {{ $dataItem->avg_db }} </td>


                                </tr>

                            @endforeach


                        @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </section>

    </div>


@endsection

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>


