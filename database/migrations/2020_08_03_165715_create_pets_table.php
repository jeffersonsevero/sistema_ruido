<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cliente');
            $table->string('nome');
            $table->string('raca');
            $table->string('idade');
            $table->string('data_nascimento');
            $table->string('data_aquisicao');
            $table->text('doencas_cronicas');
            $table->string('deficiencia');
            $table->string('imagem');
            $table->timestamps();

            $table->foreign('cliente')->references('id')->on('clientes')->onDelete('CASCADE');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
}
