<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    protected $table = "locais";



    // public function data(){

    //     return $this->hasMany(Dado::class)
    // }

    public function data(){

        $data = Dado::where("local_id", $this->id)->orderBy('created_at', 'asc')->get()->take(65);

        return $data;

    }


}
