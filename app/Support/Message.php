<?php


namespace App\Support;


class Message
{

    private $text;
    private $type;
    private $icon;

    public function getIcon()
    {
        return $this->icon;

    }

    public function getType()
    {
        return $this->type;

    }

    public function getText()
    {
        return $this->text;

    }

    public function success(string $message): Message
    {
        $this->type = 'success';
        $this->text = $message;
        $this->icon = 'icon-hand-o-up';
        return $this;

    }

    public function error(string $message): Message
    {
        $this->type = 'error';
        $this->text = $message;
        $this->icon = 'icon-times';
        return $this;

    }

    public function render()
    {
        return "<div class='message {$this->getIcon()}  {$this->getType()}'> {$this->getText()} </div>";

    }

}
