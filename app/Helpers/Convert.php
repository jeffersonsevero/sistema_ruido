<?php


namespace App\Helpers;
use Illuminate\Support\Carbon;

class Convert
{

    public static function convertDateToUS(string $date){

        $array = explode("/", $date);

        $dateUs = "{$array[2]}-{$array[1]}-{$array[0]}";

        return $dateUs;

    }


    public static function convert(string $date){

        $date = Carbon::parse($date);

        return $date->format("d/m/Y H:i:s");

    }



    public static function standardDeviation(array $set): float

    {

        $count = count($set);

        $avg = array_sum($set) / $count;

        $value1 = 0;


        foreach($set as $value){

            $i = ($value - $avg) ** 2;

            $value1 += $i;

        }


        $value1 = $value1 / $count;


        $value1 = sqrt($value1);

        return $value1;


    }


    public static function getVariance(array $array)
    {

        $avg = array_sum($array) / count($array);

        $variance = 0;
        $qtdDeviation = 0;

        foreach($array as $number){

            $deviation =  abs($number - $avg);
            $qtdDeviation++;
            $variance += $deviation ** 2;
        }

        $variance = $variance / $qtdDeviation;

        return $variance;


    }

    public static function getMedia(array $list): float
    {

        return array_sum($list) / count($list);

    }














}
