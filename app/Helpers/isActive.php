<?php

use Carbon\Carbon;

if(!function_exists('isActive')){

    function isActive($href, $class = 'active'){

        return $class = (strpos(Route::currentRouteName(), $href) === 0 ? $class : '');

    }

}

function convert_to_date_br(string $date){

    $date = Carbon::parse($date);

    return $date->format("d/m/Y H:i:s");

}

