<?php

namespace App\Abs;

use Illuminate\Support\Facades\Auth;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use stdClass;
use Symfony\Component\VarDumper\VarDumper;

class Email
{
    private $data;

    private $mail;

    private $message;

    public function __construct()
    {
        $this->mail = new PHPMailer(true);

        //setup
        $this->mail->isSMTP();
        $this->mail->setLanguage();
        $this->mail->isHTML(CONF_MAIL_OPTION_HTML);
        $this->mail->SMTPAuth = CONF_MAIL_OPTION_AUTH;
        $this->mail->SMTPSecure = CONF_MAIL_OPTION_SECURE;
        $this->mail->CharSet = CONF_MAIL_SENDER_CHARSET;



        //auth
        $this->mail->Host = CONF_MAIL_HOST;
        $this->mail->Username = CONF_MAIL_USER;
        $this->mail->Password = CONF_MAIL_PASS;
        $this->mail->Port = CONF_MAIL_PORT;

    }


    public function bootstrap(string $subject, string $message, string $toEmail, string $toName): Email
    {

        $this->data = new stdClass();

        $this->data->subject = $subject;
        $this->data->message = $message;
        $this->data->toEmail = $toEmail;
        $this->data->toName = $toName;

        return $this;
    }


    public function send($fromEmail = "jeffersonsevero08@gmail.com", $fromName = "Jeff"): bool
    {

        if (empty($this->data)) {
            $this->message->error("Erro ao enviar, favor verifique os dados");
            return false;
        }


        try {

            $this->mail->Subject = $this->data->subject;
            $this->mail->msgHTML($this->data->message);
            $this->mail->addAddress($this->data->toEmail, $this->data->toName);
            $this->mail->setFrom($fromEmail, $fromName);

            if (!empty($this->data->attach)) {
                foreach ($this->data->attach as $path => $name) {
                    $this->mail->addAttachment($path, $name);
                }
            }

            $this->mail->send();
            return true;
        } catch (Exception $exception) {
            $this->message = $exception->getMessage() . " " . $exception->getFile();
            return false;
        }
    }


    public function mail(): PHPMailer
    {
        return $this->mail;
    }


    public function message()
    {
        return $this->message;
    }


    public function attach(string $filePath, string $fileName): Email
    {


        $this->data->attach[$filePath] = $fileName;

        return $this;
    }
}
