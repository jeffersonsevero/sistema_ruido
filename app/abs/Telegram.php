<?php

namespace App\abs;

use Exception;
use TelegramBot\Api\BotApi;



class Telegram
{

    protected $idChat = 598191766;
    protected $bot;
    protected $message;




    public function __construct()
    {
        $this->bot = new BotApi(CONF_TELEGRAM_TOKEN);
    }




    public function sendMessage(string $message): bool
    {

        $send = $this->bot->sendMessage($this->idChat, $message);

        if ($send) {
            $this->message = "Mensagem enviada com sucesso";
            return true;
        }


        $this->message = new Exception();
        return false;
    }



    public function message()
    {

        return $this->message;
    }
}
