<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{


    protected $fillable = [
        '_token'
    ];

    public function pet(){
        return $this->hasOne(Pet::class, 'cliente', 'id');

    }


}
