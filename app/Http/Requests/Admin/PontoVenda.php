<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PontoVenda extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name' => 'required|min:3|max:191',
            'cnpj' => 'required|min:10|max:14',
            'razao_social' => 'required',
            'telefone' => 'required|min:8|max:14',
            'cep' => 'required',
            'endereco' => 'required',
            'numero' => 'required',
            'complemento' => '',
            'bairro' => 'required',
            'estado' => 'required',
            'cidade' => 'required',
            'whats' => 'required',
            'responsavel' => 'required',
            'email' => 'required',
            'horario_atendimento' => 'required'

        ];
    }
}
