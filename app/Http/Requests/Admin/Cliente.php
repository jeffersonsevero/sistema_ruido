<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class Cliente extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:191',
            'cpf' => 'required|min:10|max:14',
            'telefone' => 'required|min:8|max:14',
            'nome_pet' => 'required',
            'idade_pet' => 'required',
            'data_aquisicao_pet' => 'required',
            'deficiencia_pet' => '',
            'raca_pet' => 'required',
            'data_nascimento_pet' => 'required',
            'doencas_cronicas_pet' => '',
            'cep' => 'required',
            'endereco' => 'required',
            'numero' => 'required',
            'complemento' => '',
            'bairro' => 'required',
            'estado' => 'required',
            'cidade' => 'required',

        ];
    }
}
