<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class Pet extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome_pet' => "required",
            'raca_pet' => "required",
            'idade' => "required",
            'dono' => "required",
            'data_nascimento_pet' => "required",
            'data_aquisicao_pet' => "required",
            'doencas_cronicas_pet' => "required",
            'deficiencia_pet' => "required"

        ];
    }
}
