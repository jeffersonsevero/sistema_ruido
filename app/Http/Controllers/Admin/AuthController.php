<?php

namespace App\Http\Controllers\Admin;

use App\Alert;
use App\Clinica;
use App\Dado;
use App\PontoVenda;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Local;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function showLoginForm()
    {


        if(Auth::check() === true){
            return redirect()->route('admin.home');
        }

        return view('admin.index');

    }

    public function home()
    {


        $places = Local::all();
        $users = User::all();

        $now = Carbon::now();

        $data = Dado::orderBy("created_at", "desc")->get()->take(10);



        $alertToday = Alert::whereDate("created_at", $now->toDateString() )->get();



        return view('admin.dashboard')->with([
            'places' => $places,
            "users" => $users,
            "alerts" => $alertToday,
            "data" => $data

        ]);
    }


    public function login(Request $request)
    {
        if(in_array('', $request->only('email', 'password'))){
            $json['message'] = $this->message->error('Ooops, informe todos os dados para efetuar o login!')->render();
            return response()->json($json);

        }

        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            $json['message'] = $this->message->error('Ooops, parece que o seu email não é válido!')->render();
            return response()->json($json);
        }


        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if(!Auth::attempt($credentials)){
            $json['message'] = $this->message->error('Ooops, usuário e/ou senha não conferem!')->render();
            return response()->json($json);

        }
        $this->authenticated($request->getClientIp());

        $json['redirect'] = route('admin.home');
        return response()->json($json);


    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin.login');

    }

    private function authenticated($ip){

        $user = User::where('id', Auth::user()->id)->first();
        $user->last_login_at = date('Y-m-d H:i:s');
        $user->last_login_ip = $ip;

        $user->save();
    }


}
