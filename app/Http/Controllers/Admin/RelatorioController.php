<?php

namespace App\Http\Controllers\Admin;

use App\Dado;
use App\Helpers\Convert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Local;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use stdClass;

class RelatorioController extends Controller
{


    public function index()
    {

        $locais = Local::all();

        return view("admin.relatorios.index")->with([
            "locais" => $locais
        ]);
    }


    private function isMorning(Carbon $date): bool
    {
        if ($date->hour < 12) {

            return true;
        }

        return false;
    }


    public function show($local)
    {

        $local = Local::find($local);

        $maximumNoisePerLocation = 0;

        /**@var Collection $data */
        $data = $local->data();

        if($local->tipo == "espaco"){
            $maximumNoisePerLocation = 65;

        }else if($local->tipo == "industrial"){
            $maximumNoisePerLocation = 70;

        }
        else if($local->tipo == "escola"){
            $maximumNoisePerLocation = 55;
        }

        $maximumNoisePerLocation = 50;

        $dbs = [];
        $times = [];
        $max = [];

        foreach ($data as $item) {

            $dbs[] = $item->max_db;
            $times[] = "{$item->created_at->hour}:{$item->created_at->minute}:{$item->created_at->second}";
            $max[] = $maximumNoisePerLocation;
        }


        //CHART


        $chartData = new stdClass();
        $chartData->times = implode("','", $times);
        $chartData->times = "'{$chartData->times}'";
        $chartData->dbs = implode(",", $dbs);
        $chartData->standardDeviation =  number_format(Convert::standardDeviation($dbs), 2);
        $chartData->avg = number_format( Convert::getMedia($dbs), 2 );
        $chartData->max = implode(",", $max);



        return view("admin.relatorios.show")->with([
            "local" => $local,
            "data" => $data,
            "chart" => $chartData,
        ]);
    }

}
