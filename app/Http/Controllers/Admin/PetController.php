<?php

namespace App\Http\Controllers\Admin;

use App\Cliente;
use App\Helpers\Convert;
use App\Pet;
use http\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Pet as PetRequest;

class PetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $allPets = Pet::all();


        return view("admin.pets.index", [
            "pets" => $allPets
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $clientes = Cliente::all();


        return view("admin.pets.create", [
            "clientes" => $clientes
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PetRequest $request)
    {


        $pet = new Pet();

        $pet->nome = $request->nome_pet;
        $pet->cliente = $request->dono;
        $pet->idade = $request->idade;
        $pet->data_nascimento = Convert::convertDateToUS($request->data_nascimento_pet);
        $pet->data_aquisicao = Convert::convertDateToUS($request->data_aquisicao_pet);
        $pet->doencas_cronicas = $request->doencas_cronicas_pet;
        $pet->deficiencia = $request->deficiencia_pet;

        $pet->save();

        return redirect()->route("admin.pets.index");


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
