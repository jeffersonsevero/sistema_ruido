<?php

namespace App\Http\Controllers\Admin;

use App\Representante;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Representante as RepresentanteRequest;

class RepresentanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $allRepresentantes = Representante::all();

        return view('admin.representantes.index', [
            'representantes' => $allRepresentantes
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.representantes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RepresentanteRequest $request)
    {
        $representante = new Representante();

        $representante->nome = $request->name;
        $representante->cpf = $request->cpf;
        $representante->rg = $request->rg;
        $representante->telefone = $request->telefone;
        $representante->rua = $request->endereco;
        $representante->numero = $request->numero;
        $representante->bairro = $request->bairro;
        $representante->cidade = $request->cidade;
        $representante->estado = $request->estado;
        $representante->pais = 'Brasil';
        $representante->cep = $request->cep;
        $representante->complemento = $request->complemento;
        $representante->email = $request->email;
        $representante->whats = $request->whats;
        $representante->habilitacao = $request->habilitacao;
        $representante->foto = 'foto';

        $representante->save();

        return redirect()->route('admin.representantes.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $representante = Representante::find($id);

        return view('admin.representantes.edit', [
            'representante' => $representante
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RepresentanteRequest $request, $id)
    {
        $representante = Representante::find($id);

        $representante->nome = $request->name;
        $representante->cpf = $request->cpf;
        $representante->rg = $request->rg;
        $representante->telefone = $request->telefone;
        $representante->rua = $request->endereco;
        $representante->numero = $request->numero;
        $representante->bairro = $request->bairro;
        $representante->cidade = $request->cidade;
        $representante->estado = $request->estado;
        $representante->pais = 'Brasil';
        $representante->cep = $request->cep;
        $representante->complemento = $request->complemento;
        $representante->email = $request->email;
        $representante->whats = $request->whats;
        $representante->habilitacao = $request->habilitacao;
        $representante->foto = 'foto';

        $representante->save();

        return redirect()->route('admin.representantes.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $representante = Representante::find($id);

        $representante->delete();

        return redirect()->route('admin.representantes.index');
    }
}
