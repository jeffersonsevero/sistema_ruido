<?php

namespace App\Http\Controllers\Admin;

use App\Clinica;
use App\PontoVenda;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Clinica as ClinicaRequest;

class ClinicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {

        $allClinica = Clinica::all();

        return view('admin.clinicas.index',  [
            'clinicas' => $allClinica
        ]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.clinicas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClinicaRequest $request)
    {

        $clinica = new Clinica();

        $clinica->nome = $request->name;
        $clinica->razao_social = $request->razao_social;
        $clinica->cnpj = $request->cnpj;
        $clinica->responsavel = $request->responsavel;
        $clinica->telefone = $request->telefone;
        $clinica->horario_atendimento = $request->horario_atendimento;
        $clinica->rua = $request->endereco;
        $clinica->numero = $request->numero;
        $clinica->bairro = $request->bairro;
        $clinica->cidade = $request->cidade;
        $clinica->estado = $request->estado;
        $clinica->pais = 'Brasil';
        $clinica->cep = $request->cep;
        $clinica->complemento = $request->complemento;
        $clinica->email = $request->email;
        $clinica->whats = $request->whats;
        $clinica->site = $request->site;
        $clinica->servicos = $request->servicos;
        $clinica->especialidades = $request->especialidades;

        $clinica->save();

        return redirect()->route('admin.clinicas.index');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clinica = Clinica::find($id);

        return view('admin.clinicas.edit', ['clinica' => $clinica]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClinicaRequest $request, $id)
    {
        $clinica = Clinica::find($id);

        $clinica->nome = $request->name;
        $clinica->razao_social = $request->razao_social;
        $clinica->cnpj = $request->cnpj;
        $clinica->responsavel = $request->responsavel;
        $clinica->telefone = $request->telefone;
        $clinica->horario_atendimento = $request->horario_atendimento;
        $clinica->rua = $request->endereco;
        $clinica->numero = $request->numero;
        $clinica->bairro = $request->bairro;
        $clinica->cidade = $request->cidade;
        $clinica->estado = $request->estado;
        $clinica->pais = 'Brasil';
        $clinica->cep = $request->cep;
        $clinica->complemento = $request->complemento;
        $clinica->email = $request->email;
        $clinica->whats = $request->whats;
        $clinica->site = $request->site;
        $clinica->servicos = $request->servicos;
        $clinica->especialidades = $request->especialidades;

        $clinica->save();

        return redirect()->route('admin.clinicas.index');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clinica = Clinica::find($id);

        $clinica->delete();

        return redirect()->route('admin.clinicas.index');

    }
}
