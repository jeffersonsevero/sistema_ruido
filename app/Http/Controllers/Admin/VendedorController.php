<?php

namespace App\Http\Controllers\Admin;

use App\Cliente;
use App\Pet;
use App\Vendedor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Vendedor as VendedorRequest;

class VendedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todosVendedores = Vendedor::all();

        return view('admin.vendedores.index')->with(['vendedores' => $todosVendedores]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.vendedores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendedorRequest $request)
    {


        $vendedor = new Vendedor();

        $vendedor->nome = $request->name;
        $vendedor->cpf = $request->cpf;
        $vendedor->rg = $request->rg;
        $vendedor->telefone = $request->telefone;
        $vendedor->rua = $request->endereco;
        $vendedor->numero = $request->numero;
        $vendedor->bairro = $request->bairro;
        $vendedor->cidade = $request->cidade;
        $vendedor->estado = $request->estado;
        $vendedor->pais = 'Brasil';
        $vendedor->cep = $request->cep;
        $vendedor->complemento = $request->complemento;
        $vendedor->email = $request->email;
        $vendedor->whats = $request->whats;
        $vendedor->habilitacao = $request->habilitacao;
        $vendedor->banco = $request->banco;
        $vendedor->agencia = $request->agencia;
        $vendedor->conta = $request->conta;
        $vendedor->foto = 'foto';


        $vendedor->save();

        return redirect()->route('admin.vendedores.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendedor = Vendedor::find($id);


        return view('admin.vendedores.edit', [
            'vendedor' => $vendedor,

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VendedorRequest $request, $id)
    {



        $vendedor = Vendedor::find($id);



        $vendedor->nome = $request->name;
        $vendedor->email = $request->email;
        $vendedor->cpf = $request->cpf;
        $vendedor->rg = $request->rg;
        $vendedor->habilitacao = $request->habilitacao;
        $vendedor->telefone = $request->telefone;
        $vendedor->cep = $request->cep;
        $vendedor->numero = $request->numero;
        $vendedor->complemento = $request->complemento;
        $vendedor->rua = $request->endereco;
        $vendedor->bairro = $request->bairro;
        $vendedor->cidade = $request->cidade;
        $vendedor->estado = $request->estado;
        $vendedor->pais = 'Brasil';
        $vendedor->banco = $request->banco;
        $vendedor->agencia = $request->agencia;
        $vendedor->conta = $request->conta;

        $vendedor->save();

        return redirect()->route('admin.vendedores.index');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendedor = Vendedor::find($id);

        $vendedor->delete();

        return redirect()->route('admin.vendedores.index');
    }
}
