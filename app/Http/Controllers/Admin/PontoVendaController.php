<?php

namespace App\Http\Controllers\Admin;

use App\PontoVenda;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PontoVenda as PontoVendaRequest;

class PontoVendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allPontos = PontoVenda::all();

        return view('admin.pontos_venda.index', [
            'pontos' => $allPontos
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.pontos_venda.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PontoVendaRequest $request)
    {


        $pontoVenda = new PontoVenda();
        $pontoVenda->nome = $request->name;
        $pontoVenda->razao_social = $request->razao_social;
        $pontoVenda->cnpj = $request->cnpj;
        $pontoVenda->responsavel = $request->responsavel;
        $pontoVenda->telefone = $request->telefone;
        $pontoVenda->horario_atendimento = $request->horario_atendimento;
        $pontoVenda->rua = $request->endereco;
        $pontoVenda->numero = $request->numero;
        $pontoVenda->bairro = $request->bairro;
        $pontoVenda->cidade = $request->cidade;
        $pontoVenda->estado = $request->estado;
        $pontoVenda->pais = 'Brasil';
        $pontoVenda->cep = $request->cep;
        $pontoVenda->complemento = $request->complemento;
        $pontoVenda->email = $request->email;
        $pontoVenda->whats = $request->whats;

        $pontoVenda->save();


        return redirect()->route('admin.ponto-vendas.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $ponto = PontoVenda::find($id);

        return view('admin.pontos_venda.edit', [
            'ponto' => $ponto
        ]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PontoVendaRequest $request, $id)
    {

        $pontoVenda = PontoVenda::find($id);

        $pontoVenda->nome = $request->name;
        $pontoVenda->razao_social = $request->razao_social;
        $pontoVenda->cnpj = $request->cnpj;
        $pontoVenda->responsavel = $request->responsavel;
        $pontoVenda->telefone = $request->telefone;
        $pontoVenda->horario_atendimento = $request->horario_atendimento;
        $pontoVenda->rua = $request->endereco;
        $pontoVenda->numero = $request->numero;
        $pontoVenda->bairro = $request->bairro;
        $pontoVenda->cidade = $request->cidade;
        $pontoVenda->estado = $request->estado;
        $pontoVenda->pais = 'Brasil';
        $pontoVenda->cep = $request->cep;
        $pontoVenda->complemento = $request->complemento;
        $pontoVenda->email = $request->email;
        $pontoVenda->whats = $request->whats;


        $pontoVenda->save();


        return redirect()->route('admin.ponto-vendas.index');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $pontoVenda = PontoVenda::find($id);

        $pontoVenda->delete();

        return redirect()->route('admin.ponto-vendas.index');


    }
}
