<?php

namespace App\Http\Controllers\Admin;

use App\Abs\Email as AbsEmail;
use App\Clinica;
use App\Local;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Local as LocalRequest;
use Email;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;
use stdClass;
use Symfony\Component\VarDumper\VarDumper;
use App\abs\Telegram as Telegram;
use App\Alert;
use App\Dado;
use App\Helpers\Convert;
use Carbon\Carbon;
use PHPUnit\Util\Test;

class LocalController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function index()
    {
        $allLocais = Local::all();

        return view('admin.locais.index', [
            'locais' => $allLocais
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.locais.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(LocalRequest $request)
    {

        $local = new Local();

        $local->nome = $request->nome;
        $local->tipo = $request->tipo;
        $local->descricao = $request->descricao;


        $local->save();

        return redirect()->route('admin.locais.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        $local = Local::find($id);

        return view("admin.locais.show", ["local" => $local]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $local = Local::find($id);

        return view('admin.locais.edit', ['local' => $local]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(LocalRequest $request, $id)
    {
        $local = Local::find($id);

        $local->nome = $request->nome;
        $local->tipo = $request->tipo;
        $local->descricao = $request->descricao;

        $local->save();

        return redirect()->route('admin.locais.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /**@var Local */
        $local = Local::find($id);

        $data = Dado::where('local_id', $local->id)->get();

        foreach($data as $dataItem){
            $dataItem->delete();
        }
        $local->delete();

        return redirect()->route('admin.locais.index');
    }


    public function alerta(Request $request)
    {

        $db = $request->input("db");
        $local = $request->input("local");
        $type = $request->input("typeAlert");
        $localId = $request->input("local_id");

        $lastAlert = Alert::latest()->first();

        $today = Carbon::now();

        $response = new stdClass();

        $msg = "Foi identificado que no ambiente {$local} o nível de ruído chegou à {$db} decibéis em {$today->day}/{$today->month}/{$today->year} às {$today->hour}:{$today->minute}";

        // if($today->diffInMinutes($lastAlert) < 5)
        // {
        //     return;
        // }

        if ($type == "email") {

            $email = new AbsEmail();
            $email->bootstrap("Alerta sonoro",  $msg  , "jeffersontubiba@gmail.com", "Jefferson");

            if ($email->send()) {
                $response->success = true;

                $alert = new Alert();
                $alert->local_id = $localId;
                $alert->save();

                return response()->json($response);
            }
            $response->success = false;
            $response->message = $email->message();
        }
        else{

            $telegram = new Telegram();

            if($telegram->sendMessage($msg)){

                $response->success = true;

                $alert = new Alert();
                $alert->local_id = $localId;
                $alert->save();
                return response()->json($response);

            }

            $response->success = false;
            $response->message = $telegram->message();


        }



        return response()->json($response);
    }
}
