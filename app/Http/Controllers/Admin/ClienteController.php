<?php

namespace App\Http\Controllers\Admin;

use App\Cliente;
use App\Endereco;
use App\Http\Requests\Admin\User as UserRequest;
use App\Pet;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\Promise\all;
use function React\Promise\reduce;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();

        $todosClientes = User::where("id", "<>", $user->id)->get();


        return view('admin.clientes.index')->with(['clientes' => $todosClientes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {


        $cliente = new User();

        $endereco = new Endereco();

        $cliente->name = $request->name;
        $cliente->cpf = $request->cpf;
        $cliente->telefone = $request->telefone;
        $cliente->password = bcrypt($request->password);
        $cliente->email = $request->email;
        $cliente->level = "2";

        $cliente->save();


        $endereco->cep = $request->cep;
        $endereco->numero = $request->numero;
        $endereco->endereco = $request->endereco;
        $endereco->complemento = $request->complemento;
        $endereco->bairro = $request->bairro;
        $endereco->cidade = $request->cidade;
        $endereco->estado = $request->estado;
        $endereco->pais = 'Brasil';
        $endereco->user_id = $cliente->id;

        $endereco->save();

        $pet = new Pet();
        $pet->nome = $request->nome_pet;
        $pet->cliente = $cliente->id;
        $pet->raca = $request->raca_pet;
        $pet->idade = $request->idade_pet;
        $pet->data_nascimento = $request->data_nascimento_pet;
        $pet->data_aquisicao = $request->data_aquisicao_pet;
        $pet->doencas_cronicas = $request->doencas_cronicas_pet;
        $pet->deficiencia = $request->deficiencia_pet;

        $pet->save();

        return redirect()->route('admin.clientes.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $cliente = User::find($id);
        $pet = Pet::where('cliente', $cliente->id)->first();




        return view('admin.clientes.edit', [
            'cliente' => $cliente,
            'pet' => $pet
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $cliente = User::find($id);
        $pet = Pet::where('cliente', $cliente->id)->first();



        $cliente->nome = $request->name;
        $cliente->cpf = $request->cpf;
        $cliente->telefone = $request->telefone;
        $cliente->cep = $request->cep;
        $cliente->numero = $request->numero;
        $cliente->complemento = $request->complemento;
        $cliente->rua = $request->endereco;
        $cliente->bairro = $request->bairro;
        $cliente->cidade = $request->cidade;
        $cliente->estado = $request->estado;
        $cliente->pais = 'Brasil';

        $cliente->save();

        $pet->nome = $request->nome_pet;
        $pet->raca = $request->raca_pet;
        $pet->idade = $request->idade_pet;
        $pet->data_nascimento = $request->data_nascimento_pet;
        $pet->doencas_cronicas = $request->doencas_cronicas_pet;
        $pet->deficiencia = $request->deficiencia_pet;

        $pet->save();

        return redirect()->route('admin.clientes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $cliente = User::find($id);
        $pet = Pet::where('cliente', $cliente->id)->first();

        $cliente->delete();
        $pet->delete();

        return redirect()->route('admin.clientes.index');
    }
}
