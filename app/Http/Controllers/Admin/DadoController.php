<?php

namespace App\Http\Controllers\Admin;

use App\Dado;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DadoController extends Controller
{

    public function saveData(Request $request)
    {

        $dado = new Dado();

        $dado->min_db = $request->min;
        $dado->max_db = $request->max;
        $dado->avg_db = $request->avg;
        $dado->local_id = $request->local;


        $dado->save();


    }

}
