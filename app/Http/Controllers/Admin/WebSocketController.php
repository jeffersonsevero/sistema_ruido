<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Ratchet\RFC6455\Messaging\Message;
use Ratchet\RFC6455\Messaging\MessageInterface;
use Illuminate\Support\Facades\Log;

class WebSocketController extends Controller implements MessageComponentInterface
{
    private $connectionSet = [];



    public function onOpen(\Ratchet\ConnectionInterface $conn)
    {
        echo "Nova conexão aberta!" . PHP_EOL;
        array_push($this->connectionSet, $conn);
    }

    public function onClose(\Ratchet\ConnectionInterface $conn)
    {
        foreach ($this->connectionSet as $key => $connection) {
            if ($connection == $conn) {
                unset($this->connectionSet[$key]);
            }
        }
    }

    public function onError(\Ratchet\ConnectionInterface $conn, \Exception $e)
    {
        // TODO: Implement onError() method.
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {

        foreach ($this->connectionSet as $connection) {
            if ($connection !== $from) {

                if ($msg != "-Infinity") {
                    Log::info($msg);
                    $connection->send((string)$msg);
                }
            }
        }
    }
}
